const assert = require('assert');
const login = require('../../src/login')

describe('Test Login', function () {
    let testValidUser = {
        username: 'validuser',
        password: 'strongpassword'
    }

    let testInvalidUser = {
        username: 'invaliduser',
        password: 'wrongpassword'
    }

    it('[驗證密碼] 合法使用者，須回傳驗證成功', function(done) {
        let result = login.checkPsw(testValidUser.password);
        assert.deepEqual(result, true);
        done();
    });

    it('[驗證密碼] 非法使用者，須回傳驗證失敗', function(done) {
        let result = login.checkPsw(testInvalidUser.password);
        assert.deepEqual(result, false);
        done();
    });

    it('[驗證歡迎訊息] 須回傳正確訊息', function(done) {
        let msg = login.getHelloMsg(testValidUser.username);
        assert.deepEqual(msg, 'Hello, validuser! Welcome to our website.');
        done();
    });
})